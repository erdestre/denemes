using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointRace : MonoBehaviour
{
    [SerializeField] GameObject[] CheckPoints;
    int whichCheckPoint = 0;
    private void Start()
    {
        for (int i = 1; i < CheckPoints.Length; i++)
        {
            CheckPoints[i].SetActive(false);
        }
    }
    public void Next()
    {
        CheckPoints[whichCheckPoint].SetActive(false);
        whichCheckPoint++;
        if (whichCheckPoint == CheckPoints.Length - 1)
        {
            FindObjectOfType<WinLoseCondition>().GameEnding();
        }
        else CheckPoints[whichCheckPoint].SetActive(true);

    }
}
