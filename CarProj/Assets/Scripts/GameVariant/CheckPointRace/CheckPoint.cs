using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find("Car White").GetComponent<CarController>().CheckPoint = gameObject.transform;
        gameObject.transform.parent.GetComponent<CheckPointRace>().Next();
    }
}
